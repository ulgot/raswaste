#!/usr/bin/env python
# -*- coding: utf-8 -*-

N = 8

reserve = {0:'rezerwa',1:'rezerwa'}
cointainer_level_warnings = {1001:'Awaria sterownika!\n'
    ,1101:'Nieprawidłowe wskazania czujników: 100 ON, 85 OFF, 15 OFF!\n'
    ,1102:'Nieprawidłowe wskazania czujników: 100 ON, 85 OFF, 15 ON\n'
    ,1103:'Nieprawidłowe wskazania czujników: 100 ON, 85 ON, 15 OFF!\n'
    ,1104:'Nieprawidłowe wskazania czujników: 100 OFF, 85 ON, 15 OFF!\n'
    }

general_warnings = {500:'ERROR in parse_DEVICE'
    ,666:'Niezidentyfikowany błąd.\n'}

def level_warning(status):
  extra_warning = ""
  if len(status) == N:
    if status[4] == '1':
      extra_warning += cointainer_level_warnings[1001]
    elif status[5] == '1' and status[6] == '0' and status[7] == '0':
      extra_warning += cointainer_level_warnings[1101]
    elif status[5] == '1' and status[6] == '0' and status[7] == '1':
      extra_warning += cointainer_level_warnings[1102]
    elif status[5] == '1' and status[6] == '1' and status[7] == '0':
      extra_warning += cointainer_level_warnings[1103]
    elif status[5] == '0' and status[6] == '1' and status[7] == '0':
      extra_warning += cointainer_level_warnings[1104]
  return extra_warning

### description of the devices ###
### pump no 1
p1_name         = 'Pompa P1'
p1_start_byte   = '00010001'
p1_working_hex  = 'ee'
p1_working_byte = '11101110'
p1_status_byte  = '00000000'

p1_status_description = ['T_MAX_P1','OVERLEVEL','CURRENT_P1','P1_IS_ON','CONTROL_DEAD','SK_IS_100P','SK_IS_85P','SK_IS_15P']

p1_T_MAX_P1     = {1:'pompa P1 pracowała przez maksymalny czas',
                   0:'pompa P1 nie pracowała przez maksymalny czas'}
p1_OVERLEVEL    = {1:'poziom osiagnał górny próg 85% SK',
                   0:'poziom nie osiągnął górnego progu'}
p1_CURRENT_P1   = {1:'prąd płynie przez P1',
    		   0:'prąd nie płynie przez P1'}
p1_IS_ON        = {1:'P1 została wyłączona',
                   0:'P1 została włączona'}
p1_CONTROL_DEAD = {1:'awaria: czujniki poziomu źle działają, potrzebna wymiana',
                   0:'czujniki poziomu działają dobrze'}
p1_SK_IS_100P   = {1:'poziom SK jest powyżej 100%',
                   0:'poziom SK jest poniżej 100%'}
p1_SK_IS_85P    = {1:'poziom SK jest powyżej 85%',
                   0:'poziom SK jest poniżej 85%'}
p1_SK_IS_15P    = {1:'poziom SK jest powyżej 15%',
                   0:'poziom SK jest poniżej 15%'}

p1_status_state = dict(zip(p1_status_description,
  [p1_T_MAX_P1, p1_OVERLEVEL, p1_CURRENT_P1, p1_IS_ON,
   p1_CONTROL_DEAD, p1_SK_IS_100P, p1_SK_IS_85P,  p1_SK_IS_15P]
  ))

p1_warnings = cointainer_level_warnings.copy()
p1_warnings.update(general_warnings)

def parse_p1(status):
  if status.isdigit() and len(status) == N:
    p1_status = list(status)

    for i in range(N):
      p1_status[i] = p1_status_state[ p1_status_description[i] ][int(p1_status[i])]

    extra_warning = level_warning(status)

    ret = dict(zip(
      p1_status_description+['STATUS','DEVICE','BYTE'],
      p1_status+[extra_warning,p1_name,status]
      )) 

  else:
    ret = {500:general_warnings[500]}

  return ret

### container no 1
os1_name         = 'Zbiornik OS1'
os1_working_hex  = 'dd'
os1_working_byte = '11011101'
os1_status_byte  = '00000000'

os1_status_description = ['REZERWA','REZERWA','REZERWA','REZERWA','CONTROL_DEAD','OS1_IS_100P','OS1_IS_85P','OS1_IS_15P']

os1_REZERWA1     = reserve.copy()
os1_REZERWA2     = reserve.copy()
os1_REZERWA3     = reserve.copy()
os1_REZERWA4     = reserve.copy()
os1_CONTROL_DEAD = {1:'awaria: czujniki poziomu źle działają, potrzebna wymiana',
                    0:'czujniki poziomu działają dobrze'}
os1_IS_100P   = {1:'poziom OS1 jest powyżej 100%',
                    0:'poziom OS1 jest poniżej 100%'}
os1_IS_85P    = {1:'poziom OS1 jest powyżej 85%',
                    0:'poziom OS1 jest poniżej 85%'}
os1_IS_15P    = {1:'poziom OS1 jest powyżej 15%',
                    0:'poziom OS1 jest poniżej 15%'}

os1_status_state = dict(zip(os1_status_description,
    [os1_REZERWA1, os1_REZERWA2, os1_REZERWA3, os1_REZERWA4,
         os1_CONTROL_DEAD, os1_IS_100P, os1_IS_85P,  os1_IS_15P]
      ))

os1_warnings = cointainer_level_warnings.copy()
os1_warnings.update(general_warnings)

def parse_os1(status):
  if status.isdigit() and len(status) == N:
    os1_status = list(status)

    for i in range(N):
      os1_status[i] = os1_status_state[ os1_status_description[i] ][int(os1_status[i])]
      
    extra_warning = level_warning(status)
    	
    ret = dict(zip(
      os1_status_description+['STATUS','DEVICE','BYTE'],
      os1_status+[extra_warning,os1_name,status]
      ))

  else:
    ret = {500:general_warnings[500]}
    
  return ret

### container no 2
os2_name         = 'Zbiornik OS2'
os2_working_hex  = 'cc'
os2_working_byte = '11001100'
os2_status_byte  = '00000000'

os2_status_description = ['REZERWA','REZERWA','REZERWA','REZERWA','CONTROL_DEAD','OS2_IS_100P','OS2_IS_85P','OS2_IS_15P']

os2_REZERWA1     = reserve.copy()
os2_REZERWA2     = reserve.copy()
os2_REZERWA3     = reserve.copy()
os2_REZERWA4     = reserve.copy()
os2_CONTROL_DEAD = {1:'awaria: czujniki poziomu źle działają, potrzebna wymiana',
                    0:'czujniki poziomu działają dobrze'}
os2_IS_100P   = {1:'poziom OS2 jest powyżej 100%',
                    0:'poziom OS2 jest poniżej 100%'}
os2_IS_85P    = {1:'poziom OS2 jest powyżej 85%',
                    0:'poziom OS2 jest poniżej 85%'}
os2_IS_15P    = {1:'poziom OS2 jest powyżej 15%',
                    0:'poziom OS2 jest poniżej 15%'}

os2_status_state = dict(zip(os2_status_description,
    [os2_REZERWA1, os2_REZERWA2, os2_REZERWA3, os2_REZERWA4,
         os2_CONTROL_DEAD, os2_IS_100P, os2_IS_85P,  os2_IS_15P]
      ))

os2_warnings = cointainer_level_warnings.copy()
os2_warnings.update(general_warnings)

def parse_os2(status):
  if status.isdigit() and len(status) == N:
    os2_status = list(status)

    for i in range(N):
      os2_status[i] = os2_status_state[ os2_status_description[i] ][int(os2_status[i])]
      
    extra_warning = level_warning(status)
    	
    ret = dict(zip(
      os2_status_description+['STATUS','DEVICE','BYTE'],
      os2_status+[extra_warning,os2_name,status]
      ))

  else:
    ret = {500:general_warnings[500]}
    
  return ret

### TODO...



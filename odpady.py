
no_of_par = 5
parnames  = ["parametr %d"%i for i in range(no_of_par)]
params    = dict(zip(parnames,[0]*no_of_par))


def parse_rcv(rcv, params):
  _rcv = rcv.strip()
  j = 1
  for i in _rcv:
    params["parametr %d"%j] = i
    j += 1
  return params

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import HTMLgen

def table_html(params):
  ret = "\n\n<h1>%s</h1>\n"%params['DEVICE']
  ret += "<table border=1>\n"
  for k, v in params.iteritems():
    if k == 'STATUS':
      if not v:
	ret += "<tr style='background-color:green;'><td>%s</td><td>System OK</td></tr>\n"%k
      else:
        ret += "<tr style='background-color:red;'><td>%s</td><td>%s</td></tr>\n"%(k,v)
    elif k == 'DEVICE':
      pass
    else:
      ret += "<tr><td>%s</td><td>%s</td></tr>\n"%(k,v)
  return ret + "</table>\n\n"

def status_html(params):
  info = "\n\n<h1>%s</h1>\n"%params['DEVICE']
  ret = "<ul>\n"
  for k, v in params.iteritems():
    if k == 'STATUS':
      if not v:
	info += "<h2 style='color:green;'>%s: System OK</h2>\n"%k
      else:
	info += "<h2 style='color:red;'>%s: %s</h2>\n"%(k,v)
    elif k == 'DEVICE' or k == 'REZERWA':
      pass
    else:
      ret += "<li>%s: %s</li>\n"%(k,v)
  return info + ret + "</ul>\n\n"

#info += '<div class="dev_box_status_%s">%s</div>\n'%("ok" if not status else "notok","System OK" if not status else status)
def status_css(params):
  status = params['STATUS']
  info = '\n\n<div class="box">\n<div class="dev_box_head">%s</div>\n'%params['DEVICE']
  if not status:
    system = '<div class="dev_box_status_ok">System OK</div>\n'
  else:
    system = '<div class="dev_box_status_notok">%s</div>\n'%status
  ret = '<div class="dev_box">\n<ul>\n'
  for k, v in params.iteritems():
    if k == 'STATUS' or k == 'DEVICE' or k == 'REZERWA': 
      pass
    elif k[-4:] == '100P' and params['BYTE'][5] == '1' and not status:
      system = '<div class="dev_box_status_full">Przepełniony zbiornik!</div>\n'
      ret += "<li>%s</li>\n"%(v)
    else:
      ret += "<li>%s</li>\n"%(v)
      #ret += "<li>%s: %s</li>\n"%(k,v)
  return info + system + ret +"</ul>\n</div>\n</div>"

def index_head():
  ret = """<html>
  <head>
    <title>Diagnostyka oczyszczalni</title>
    <meta http-equiv="refresh" content="3"/>
    <meta charset="UTF-8"/>
    <meta name="description" content="oczyszczalnia, diagnostyka"/>
    <meta name="author" content="LM"/>
    <link href="style.css" rel="stylesheet" type="text/css"/>
  </head>
  <body>
"""
  return ret

def index_foot():
  return "\n\n</body>\n</html>"









### HTMLGen
def index(content):
  doc = HTMLgen.SimpleDocument(title="Diagnostyka oczyszczalni")
  doc.append(HTMLgen.Heading(1,"Poziomy czujników"))
  doc.append(content)
  print doc



#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
import random
import HTMLgen
import time

from devices import *
from htmlfy import *

###################################################################
def readusb():
  port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=1.0)
  #port = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=3.0)
  rcv = port.read(100)
  return rcv

def generate_message():
  return ''.join([str(random.randint(0,1)) for i in range(N)])

devbin = {'P1':p1_working_byte
    ,'OS1':os1_working_byte
    ,'OS2':os2_working_byte
    }

if __name__ == "__main__":
  while True:
    time.sleep(3)

    #make index.html
    index_source = index_head()
    for _device in devbin.values():
      message = generate_message()
      if _device == devbin['P1']:
	index_source += status_css(parse_p1(message))
      elif _device == devbin['OS1']:
	index_source += status_css(parse_os1(message))
      elif _device == devbin['OS2']:
	index_source += status_css(parse_os2(message))
    index_source += index_foot()

    try:
      with open('/var/www/index.html','w') as thefile:
        thefile.write(index_source)
    except:
      with open('index.html','w') as thefile:
        thefile.write(index_source)
      

#!/usr/bin/env python
# -*- coding: utf-8 -*-

import serial
import random
import HTMLgen
import time

N = 8
### description of the devices ###
### pump no 1
p1_name = 'Pompa P1'
p1_start_byte = "00010001"
p1_wroking_byte = "11101110"
p1_status_byte = "00000000"
p1_status_description = ['T_MAX_P1','OVERLEVEL','CURRENT_P1','P1_IS_ON','CONTROL_DEAD','SK_IS_100P','SK_IS_85P','SK_IS_15P']
p1_T_MAX_P1     = {1:'pompa P1 pracowała przez maksymalny czas',
                   0:'pompa P1 nie pracowała przez maksymalny czas'}
p1_OVERLEVEL    = {1:'poziom osiagnał górny próg 85% SK',
                   0:'poziom nie osiągnął górnego progu'}
p1_CURRENT_P1   = {1:'prąd płynie przez P1',
    		   0:'prąd nie płynie przez P1'}
p1_IS_ON        = {1:'P1 została wyłączona',
                   0:'P1 została włączona'}
p1_CONTROL_DEAD = {1:'awaria: czujniki poziomu źle działają, potrzebna wymiana',
                   0:'czujniki poziomu działają dobrze'}
p1_SK_IS_100P   = {1:'poziom SK jest powyżej 100%',
                   0:'poziom SK jest poniżej 100%'}
p1_SK_IS_85P    = {1:'poziom SK jest powyżej 85%',
                   0:'poziom SK jest poniżej 85%'}
p1_SK_IS_15P    = {1:'poziom SK jest powyżej 15%',
                   0:'poziom SK jest poniżej 15%'}
p1_status_state = dict(zip(p1_status_description,
  [p1_T_MAX_P1, p1_OVERLEVEL, p1_CURRENT_P1, p1_IS_ON,
   p1_CONTROL_DEAD, p1_SK_IS_100P, p1_SK_IS_85P,  p1_SK_IS_15P]
  ))
p1_warnings = {1001:'Awaria czujników poziomu!\n'
    ,1101:'Nieprawidłowe wskazania czujników: 100 ON, 85 OFF, 15 OFF!\n'
    ,1102:'Nieprawidłowe wskazania czujników: 100 ON, 85 OFF, 15 ON\n'
    ,1103:'Nieprawidłowe wskazania czujników: 100 ON, 85 ON, 15 OFF!\n'
    ,1104:'Nieprawidłowe wskazania czujników: 100 OFF, 85 ON, 15 OFF!\n'
    ,500:'ERROR in parse_p1'
    ,666:'Niezidentyfikowany błąd.\n'}
def parse_p1(status):
  if status.isdigit() and len(status) == N:
    p1_status = list(status)
    for i in range(N):
      p1_status[i] = p1_status_state[ p1_status_description[i] ][int(p1_status[i])]

    extra_warning = ""
    if status[4] == '1':
      extra_warning += p1_warnings[1001]
    elif status[5] == '1' and status[6] == '0' and status[7] == '0':
      extra_warning += p1_warnings[1101]
    elif status[6] == '1' and status[6] == '0' and status[7] == '1':
      extra_warning += p1_warnings[1102]
    elif status[6] == '1' and status[6] == '1' and status[7] == '0':
      extra_warning += p1_warnings[1103]
    elif status[6] == '0' and status[6] == '1' and status[7] == '0':
      extra_warning += p1_warnings[1104]

    ret = dict(zip(
      p1_status_description+['STATUS','DEVICE'],
      p1_status+[extra_warning,p1_name]
      )) 
  else:
    ret = {500:p1_warnings[500]}

  return ret
### container no 1
### TODO...


###################################################################
def readusb():
  port = serial.Serial("/dev/ttyUSB0", baudrate=9600, timeout=1.0)
  #port = serial.Serial("/dev/ttyAMA0", baudrate=9600, timeout=3.0)
  rcv = port.read(100)
  return rcv

def status_html(params):
  info = "\n\n<h1>%s</h1>\n"%params['DEVICE']
  ret = "<ul>\n"
  for k, v in params.iteritems():
    if k == 'STATUS':
      if not v:
	info += "<h2 style='color:green;'>%s: System OK</h2>\n"%k
      else:
	info += "<h2 style='color:red;'>%s: %s</h2>\n"%(k,v)
    elif k == 'DEVICE':
      pass
    else:
      ret += "<li>%s: %s</li>\n"%(k,v)
  return info + ret + "</ul>\n\n"

def myindex(content):
  ret = """<html>
  <head>
    <title>Diagnostyka oczyszczalni</title>
    <meta http-equiv="refresh" content="3">
    <meta charset="UTF-8">
    <meta name="description" content="oczyszczalnia, diagnostyka">
    <meta name="author" content="LM">
  </head>
  <body>
  %s
  </body>
</html>
"""%content
  return ret



def text_html(params):
  ret = "\n\n<h1>%s</h1>\n"%params['DEVICE']
  ret += "<table border=1>\n"
  for k, v in params.iteritems():
    if k == 'STATUS':
      if not v:
	ret += "<tr style='background-color:green;'><td>%s</td><td>System OK</td></tr>\n"%k
      else:
        ret += "<tr style='background-color:red;'><td>%s</td><td>%s</td></tr>\n"%(k,v)
    elif k == 'DEVICE':
      pass
    else:
      ret += "<tr><td>%s</td><td>%s</td></tr>\n"%(k,v)
  return ret + "</table>\n\n"

def myindex(content):
  ret = """<html>
  <head>
    <title>Diagnostyka oczyszczalni</title>
    <meta http-equiv="refresh" content="3">
    <meta charset="UTF-8">
    <meta name="description" content="oczyszczalnia, diagnostyka">
    <meta name="author" content="LM">
  </head>
  <body>
  %s
  </body>
</html>
"""%content
  return ret

def index(content):
  doc = HTMLgen.SimpleDocument(title="Diagnostyka oczyszczalni")
  doc.append(HTMLgen.Heading(1,"Poziomy czujników"))
  doc.append(content)
  print doc

def generate_message():
  return ''.join([str(random.randint(0,1)) for i in range(N)])

if __name__ == "__main__":
  while True:
    time.sleep(3)
    
    message = generate_message()
    #print message
    params = parse_p1(message)
    #print params
    with open('index.html','w') as thefile:
      thefile.write(myindex(status_html(params)))
